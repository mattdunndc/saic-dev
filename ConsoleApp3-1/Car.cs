﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3_1
{
    public class Car
    {
        public string year { get; set; }
        public string make { get; set; }
        public string model { get; set; }
        public IList<Plate> plates { get; set; }
    }
}
